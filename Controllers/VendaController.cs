using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController: ControllerBase
    {
        private readonly OrganizadorContext _context;

        public VendaController(OrganizadorContext context){
            _context = context;
        }

        [HttpPost("Cadastrar")]
         public ActionResult<Venda> Post([FromBody] Venda venda){ 
            if(venda.Status != "Aguardando Pagamento")
            return NotFound(new
            {
                Error = "Status não aceito"
            });
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return Created("Criado", venda);
        }

        [HttpGet("Buscar/{idVenda}")]
        public ActionResult<Venda> GetVenda(int idVenda){
            var venda = _context.Vendas.Find(idVenda);
            if(venda == null)
            return NotFound(new
                {
                    Error = "A Venda não foi encontrada"
                });
            var listas = _context.Vendas
            .Include(v => v.Produtos)
            .Include(v => v.Vendedor);
            listas.OrderBy(i => i.Id);
            
            return  listas.FirstOrDefault(v => v.Id == idVenda);    
        }

        [HttpPut("Atualizar/{idVenda}")]
        public IActionResult AtualizarVenda([FromRoute]int idVenda, [FromBody] Venda venda){
            var vendaBanco = _context.Vendas.Find(idVenda);
            
            while(vendaBanco.Status == "Aguardando Pagamento"){
                if(venda.Status == "Pagamento Aprovado" || venda.Status == "Cancelada"){
                    vendaBanco.Status = venda.Status;
                }else{
                    return NotFound(new{Error = "Status não aceito"});
                }
                _context.Update(vendaBanco);
                _context.SaveChanges();
                 return Ok(vendaBanco);
            }
          
            while(vendaBanco.Status == "Pagamento Aprovado"){
                if(venda.Status == "Enviado para Transportadora" || venda.Status == "Cancelada"){
                    vendaBanco.Status = venda.Status;
                }else{
                    return NotFound(new{Error = "Status não aceito"});
                }
                _context.Update(vendaBanco);
                _context.SaveChanges();
                 return Ok(vendaBanco);
            }

            while(vendaBanco.Status == "Enviado para Transportadora"){
                if(venda.Status == "Entregue" ){
                    vendaBanco.Status = venda.Status;
               }else{
                    return NotFound(new{Error = "Status não aceito"});
               }
                _context.Update(vendaBanco);
                _context.SaveChanges();
                 return Ok(vendaBanco);
            }
               
                return Ok(vendaBanco);
        }
    }
}
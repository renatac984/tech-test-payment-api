openapi: 3.0.1
info:
  title: defaultTitle
  description: defaultDescription
  version: '0.1'
servers:
  - url: https://localhost:7020
paths:
  /Venda/Atualizar/17:
    put:
      description: Auto generated using Swagger Inspector
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                vendedor:
                  type: object
                  properties:
                    telefone:
                      type: string
                    cpf:
                      type: string
                    nome:
                      type: string
                    id:
                      type: integer
                    email:
                      type: string
                data:
                  type: string
                produtos:
                  type: array
                  items:
                    type: object
                    properties:
                      vendaId:
                        type: integer
                      id:
                        type: integer
                      nomeProduto:
                        type: string
                id:
                  type: integer
                status:
                  type: string
            examples:
              '0':
                value: "{\r\n  \"id\": 0,\r\n  \"status\": \"Pagamento Aprovado\",\r\n  \"data\": \"2023-01-19T18:34:16.621Z\",\r\n  \"vendedor\": {\r\n    \"id\": 0,\r\n    \"nome\": \"Marlene\",\r\n    \"cpf\": \"98302837263\",\r\n    \"telefone\": \"21976382\",\r\n    \"email\": \"marlene.com\"\r\n  },\r\n  \"produtos\": [\r\n    {\r\n      \"id\": 0,\r\n      \"vendaId\": 0,\r\n      \"nomeProduto\": \"fones de ouvido\"\r\n    }\r\n  ]\r\n}"
      responses:
        '200':
          description: Auto generated using Swagger Inspector
          content:
            application/json; charset=utf-8:
              schema:
                type: string
              examples: {}
      servers:
        - url: https://localhost:7020
    servers:
      - url: https://localhost:7020
  /Venda/Buscar/17:
    get:
      description: Auto generated using Swagger Inspector
      responses:
        '200':
          description: Auto generated using Swagger Inspector
          content:
            application/json; charset=utf-8:
              schema:
                type: string
              examples: {}
      servers:
        - url: https://localhost:7020
    servers:
      - url: https://localhost:7020
  /Venda/Cadastrar:
    post:
      description: Auto generated using Swagger Inspector
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                vendedor:
                  type: object
                  properties:
                    telefone:
                      type: string
                    cpf:
                      type: string
                    nome:
                      type: string
                    id:
                      type: integer
                    email:
                      type: string
                data:
                  type: string
                produtos:
                  type: array
                  items:
                    type: object
                    properties:
                      vendaId:
                        type: integer
                      id:
                        type: integer
                      nomeProduto:
                        type: string
                id:
                  type: integer
                status:
                  type: string
            examples:
              '0':
                value: "{\r\n  \"id\": 0,\r\n  \"status\": \"Aguardando Pagamento\",\r\n  \"data\": \"2023-01-19T18:34:16.621Z\",\r\n  \"vendedor\": {\r\n    \"id\": 0,\r\n    \"nome\": \"Marlene\",\r\n    \"cpf\": \"98302837263\",\r\n    \"telefone\": \"21976382\",\r\n    \"email\": \"marlene.com\"\r\n  },\r\n  \"produtos\": [\r\n    {\r\n      \"id\": 0,\r\n      \"vendaId\": 0,\r\n      \"nomeProduto\": \"fones de ouvido\"\r\n    }\r\n  ]\r\n}"
      responses:
        '200':
          description: Auto generated using Swagger Inspector
          content:
            application/json; charset=utf-8:
              schema:
                type: string
              examples: {}
      servers:
        - url: https://localhost:7020
    servers:
      - url: https://localhost:7020
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Context
{
    public class OrganizadorContext:DbContext
    {
        public OrganizadorContext(DbContextOptions<OrganizadorContext> options): base(options){

        }

        public DbSet<Venda> Vendas { get; set; }
        public DbSet<Vendedor> Vendedores { get; set; }
        public DbSet<Produto> Produtos { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.Entity<Venda>(tabela =>{
                tabela.HasKey(e => e.Id);
                tabela
                    .HasMany(e => e.Produtos)
                    .WithOne()
                    .HasForeignKey(c => c.VendaId);
            });
            builder.Entity<Vendedor>(tabela =>
            {
                tabela.HasKey(e => e.Id);
            });
            builder.Entity<Produto>(tabela =>
            {
                tabela.HasKey(e => e.Id);
            });
        }
    }
}
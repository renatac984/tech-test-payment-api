namespace tech_test_payment_api.Models
{
    public class Produto
    {
        public int Id { get; set; }
        public int VendaId { get; set; }
        public string NomeProduto { get; set; }
    }
}
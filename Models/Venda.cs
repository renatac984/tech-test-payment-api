using System.Collections.Generic;
using System.Globalization;

namespace tech_test_payment_api.Models
{
    public class Venda
    {
        public Venda(){
            this.Produtos = new List<Produto>();
        }
        public int Id { get; set; }
        public string Status { get; set; }
        public DateTime Data {get; set;} = DateTime.Now;
        public Vendedor Vendedor { get; set; }
        public List<Produto> Produtos { get; set; }
    }
}